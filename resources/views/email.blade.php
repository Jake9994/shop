@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


        <div class="card">
            <div class="card-header">Email Test with Queue</div>
            <div class="card-body">

                <form class="form-group">
                    @csrf
                    <label for="email_txt">Email:</label>
                    <input class="form-control" type="email" name="email_txt" id="email_txt">
                    <input type="submit" class="btn btn-primary button submit_btn" value="Submit">
                </form>
            </div>
        </div>


        </div>
    </div>
</div>

<script type="text/javascript">
    
$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.submit_btn').click(function(e){

        e.preventDefault();

        var email = $('#email_txt').val();
        // console.log(email);
        data = {
            'email' : email
        }
        // console.log(data);
        $.ajax({
            type:"POST",
            url: "{{route('sendmail')}}",
            data:data,
            dataType:"json",
            success: function(data)
            {
                if(data.success)
                {
                    alert(data.success);
                }
                if(data.error)
                {
                    alert(data.error);
                }
            },
            error:function(data){
                console.log(data);
                console.log(data.responseText);
            }
        });
        
    })

})

</script>
@endsection
