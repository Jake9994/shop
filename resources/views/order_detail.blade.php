@extends('layouts.app')

@section('content')



    <div class="col-md-offset-1">
        <div class="card-group">
          <div class="card">
            <img class="card-img-top order_fix_size_image" src="http://shop.test/{{$product_info->img_url}}" alt="Card image cap">
            <div class="card-body left-panel">
              <h5 class="card-title" style="font-size: 3em">{{$product_info->name}} - {{$product_info->brand}}</h5>
              <p class="card-text" style="font-size: 3em"><h5>RM {{$cp = $product_info->current_price}}</h5>
                <span class="discount" style="font-size: 3em">Price {{$product_info->retail_price}}</span>
              </p>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <p class="card-text">
                <div class="form-group">
                    <input type="hidden" value="{{$id = $product_info->id}}">
                    <label class="">Discount Code:</label>
                    <input class="form-control" type="text" id="discount_txt">
                    <button class="btn btn-success" id="discount_apply">Apply</button>
                    <br/>
                    <br/>
                   <label>Shipping Country:</label>
                    <select class="form-control" id="shipping"  name="amount">
                        <option value="none">Select Country</option>
                        <option value="1">Malaysia</option>
                        <option value="2">Singapore</option>
                        <option value="3">Brunei</option>
                    </select>
                </div>
                <br/>              

                <table class="order_table" style="font-size: 3em">
                    <tr>
                        <td class="td_left">Price(RM):</td>
                        <td class="td_right">{{$product_info->current_price}}</td>
                    </tr>
                    <tr class="underline">
                        <td class="td_left">Quantity:</td>
                        <td class="td_right">{{$amt = $amount_arr['amount']}}</td>
                    </tr>
                    <tr>
                        <td class="td_left">Total:</td>
                        <td class="td_right"><div class="first_total">{{($product_info->current_price * $amount_arr['amount'])}}</div></td>
                    </tr>
                    <tr>
                        <td class="td_left">Discount:</td>
                        <td class="td_right"><div class="discount_num">-</div></td>
                    </tr>
                    <tr class="underline">
                        <td class="td_left">Shipping Fee(RM):</td>
                        <td class="td_right"><div class="shipping_fee"></div></td>
                    </tr>
                    <tr>
                        <td class="td_left">Total(RM):</td>
                        <td class="td_right"><div class="total_price">
                            {{($product_info->current_price * $amount_arr['amount'])}}
                        </td>
                    </tr>
                </table>
              </p>

            </div>
            <form class="checkout_form">
                @csrf
                <input type="submit" class="btn btn-success" id="checkout" value="Check Out">
            </form>
            
          </div>
        </div>
    </div>


<script type="text/javascript">
$(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var item_price_raw = '<?= $cp ?>';
    var item_price = parseFloat(item_price_raw).toFixed(2);
    var amount = '<?= $amt ?>';
    var total = (item_price*amount);
    var my_fee = 10;
    var sg_fee = 20;
    var bn_fee = 25;
    //Type 1 = 5%, minimum 2 amount
    //Type 2 = RM 15, minimum RM100
    var discount_id = null;
    var discount_type = 0;
    var discount_display = null;
    var discount_num = 0;
    var discount_amount = 0;
    var final_total = 0;
    var code = null;
    var shipping_rate = 0;

    $("#discount_apply").click(function(e){
        code = $('#discount_txt').val();

        if(code){
            var data = {
                'discount_code':code,
                'product_id':{{$product_info->id}},
                'quantity':<?= $amt ?>,
                'shipping_rate': shipping_rate
            };
            $.ajax({
               type: "POST",
               url: "{{ route('check_discount') }}",
               data: data, 
                dataType: 'json',
                // cache: false,
               success: function(data)
               {
                if(data.error){
                    alert(data.error);
                }

                if(data.name){
                    discount_id = data.id;
                    discount_type = data.discount_type;
                    discount_amount = data.discount_amount;
                    discount_display = $('.discount_num').text(data.discount_display);
                    final_total = $('.total_price').text(data.sum_total);
                }

               }
             });
        }
    })


    $("#shipping").change(function(){
        code = $('#discount_txt').val();

        if(discount_type == 1){
            discount_num = (final_total*5)/100;
        }else if(discount_type == 2){
            discount_num = discount_amount;
        }else{
            discount_num = discount_amount;
        }

        var data={
            'product_id':'{{$product_info->id}}',
            'quantity':'<?= $amt ?>',
            'discount_code':code,
            'country':this.value
        };

        $.ajax({
            type:"POST",
            url: "{{route('check_shipping')}}",
            data:data,
            dataType:"json",
            success:function(data)
            {
                if(data.error){
                    alert(data.error);
                }
                if(data.success){
                    $(".shipping_fee").text(data.shipping_rate);
                    $(".total_price").text(twodec(data.total));
                    shipping_rate = data.shipping_rate;
                }
            }
        });

        var ddl = $('#shipping').val();
        if(ddl = 'none'){
            shipping_rate = 0;
            var refresh_total = total - discount_amount;
            $(".shipping_fee").text('');
            $(".total_price").text(twodec(refresh_total));
        }
    })

    // $('.checkout_form').submit(function(e){
    $('#checkout').click(function(e){

            code = $('#discount_txt').val();

            var ship_country = $("#shipping").val();
            if(ship_country == 'none'){
                alert('Please select a country');
            }else{

                var product_id = '<?= $id ?>';
                var country_id = ship_country;
                var selected_country = $('#shipping').val();
                var fdata = {
                    'product_id' : product_id,
                    'quantity':amount,
                    'discount_code':code,
                    'country':selected_country,
                    'code': code
                }
                ;
                // console.log(fdata);

                $.ajax({
                       type: "POST",
                       url: "{{route('process_checkout')}}",
                       data: fdata, // serializes the form's elements.
                        dataType: 'json',
                        // cache: false,
                       success: function(data)
                       {
                        console.log(data);
                            alert ('Success to purchase');
                            window.location.href = '/summary/'+data.last_inset_id;
                            // console.log(data.last_inset_id);
                            // console.log(data['result']);
                            // if(data['result'] == true){
                            //   console.log('yes');
                            // }
                       }
                     });

                e.preventDefault(); // avoid to execute the actual submit of the form.
            }
    })

    function twodec($num){
        var result = parseFloat($num).toFixed(2);
        return result;
    }
})
</script>
@endsection
