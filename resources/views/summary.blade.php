@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card" style="text-align: center;">
                <div class="card-header">Purchase Summary</div>

                <div class="card-body">
                    <img src="http://shop.test/{{$order_info->product->img_url}}" width="50%" height="auto">
                    <p>{{$order_info->product->name}} - {{$order_info->product->brand}}</p>
                    <p>Price: RM {{$order_info->product->current_price}}</p>
                    <p>Amount: {{$order_info->amount}}</p>
                    <p>Discount: -RM {{$order_info->discount_amount}}</p>
                    <p>Shipping Fee: RM {{$order_info->shipping_fee}}</p>
                    <p>Shipping To: {{$order_info->country->name}}</p>
                    <p>Total: RM {{$order_info->total}}</p>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
