@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        	@foreach($order_history as $oh)
            <div class="card">
                <div class="card-header">{{$oh->id}}</div>
                <div class="card-body">
                    <p>Purchase Date: {{$oh->created_at}}</p>
                	<p>{{$oh->product->name}} - {{$oh->product->brand}}</p>
                	<p> RM {{$oh->total}}</p>    
                	<a href="/summary\{{$oh->id}}">View</a>          		
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection
