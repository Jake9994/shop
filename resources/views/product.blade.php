@extends('layouts.app')

@section('content')

    <!-- Product Listing -->
    <div class="container">

      <div class="row">
        @if(!empty($product_list))

        @foreach($product_list as $product)
             

        <div class="col-sm item-list">
            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{$product['img_url']}}" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title product_title">{{$product['name']}} - {{$product['brand']}}</h5>
                <p class="card-text">
                    <div class="in-same-line">
                        <h5 class="price">RM {{$product['current_price']}}</h5>
                        <span class="discount">RM {{$product['retail_price']}}</span>
                    </div>
                </p>
                <p class="card-text">{{$product['desc']}}</p>
                <form class="post_form">
                    {{ csrf_field() }}
                    <input type="hidden" name="item_id" value="{{$product['id']}}">
                    <div class="form-inline" style="display:inline;">
                        <select class="form-control"  name="amount">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                        <input type="submit" class="btn btn-success" value="Purchase" class="submit_btn">
                    </div>
                </form>
              </div>
            </div>
        </div>

        @endforeach

        @endif
<!-- Footer -->
<script type="text/javascript">

$(function(){
    $(".post_form").submit(function(e) {
        // alert ('triggered');

        var form = $(this);

        var url = form.attr('action');

        $.ajax({
               type: "POST",
               url: "{{route('order_detail')}}",
               data: form.serialize(), // serializes the form's elements.
               success: function(data)
               {
                  window.location.href = '/order/'+data.item_id+'/'+data.amount;
               }
             });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
})


</script>

@endsection
