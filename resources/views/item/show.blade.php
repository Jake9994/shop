@extends('layouts.app')

@section('content')
<div class="">
    <h2>Product Name: </h2>
    <p>{{ $item->name }} || ${{ money_format($item->price, 2) }}</p>

    <h3>Product Belongs to</h3>

    <ul>
        @foreach($item->categories as $category)
        <li>{{ $category->title }}</li>
        @endforeach
    </ul>
</div>

@endsection
    