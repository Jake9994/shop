<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Mail\SendMailable;
use Carbon\Carbon;
use Carbon\addSeconds;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
	public function index()
	{
		return view('email');
	}

    public function sendEmail(Request $request)
    {

    	$email = $request->email;
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);


		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	    	// $testresult = Mail::to('wj.liew@nettium.net')->send(new SendMailable());
		   $emailJob = (new SendEmailJob($email))->delay(Carbon::now()->addSeconds(3));
		   dispatch($emailJob);
		   // echo Carbon::now('Asia/Singapore');
		   // echo 'email sent';
		   $result = ['success'=>'Successful add into queue, will send the message soon'];
		   return response()->json($result);
		}

		$result = ['error'=>'Invalid Email'];
	   	return response()->json($result);
    }
}
