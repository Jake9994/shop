<?php

namespace App\Http\Controllers;

use App\Country;
use App\Discount_code;
use App\Order_history;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;



class OrderController extends Controller
{
    public function order(Request $request){
    	return $request;
    }

    public function order_info($id,$amount){

    	$product_info = Product::where('id',$id)
    				->first();
        // Log::info($product_info);
    	$amount_arr = ['amount' => $amount];

    	return view('order_detail')->with('product_info',$product_info)->with('amount_arr', $amount_arr);
    }

    public function p_checkout(Request $request){
        $arr = $request->all();
        $product = Product::where('id',$request->product_id)->first();
        $country = Country::where('id',$request->country)->first();
        if($request->code){
            $discount = Discount_code::where('name',$request->code)->first();
            $discount_id = $discount->id;
            $discount_type = $discount->discount_type;
        }else{
            //In this case id 3 consider no discount
            $discount_id = 3;
            $discount_type = 2;
        }
        $quantity = $request->quantity;
        $sum_total = ($product->current_price * $quantity);

        if($discount_id != 3 ){
            if($discount_id == 1)
            {
                $discount_amount = (($product->current_price * $quantity) * $discount->discount_amount)/100;

            }else if($discount_id == 2)
            {
                $discount_amount = $discount->discount_amount;
            }
        }else{
            $discount_amount = 0.00;
        }

        if(($sum_total > $country->free_delivery_amount && $country->free_delivery_amount_status == 1) || ($quantity >= $country->free_delivery_unit && $country->free_delivery_unit_status == 1))
        {
            $shipping_fee = 0;
        }
        else
        {
            $shipping_fee = $country->shipping_rate;
        }
        Log::info($discount_amount);
        $final_total = ($sum_total - $discount_amount) + $shipping_fee;

        $data = new Order_history;
        $data->users_id = Auth::id();
        $data->product_id = $request->product_id;
        $data->amount = $request->quantity;
        $data->discount_id = $discount_id; 
        $data->discount_amount =$discount_amount;
        $data->country_id = $request->country;
        $data->shipping_fee = $shipping_fee;
        $data->total = $final_total;

        $data->save();

        return response()->json(['result'=> TRUE, 'last_inset_id'=>$data->id]);
        // return response()->json($data);
    }

    public function check_discount(Request $request){
        $discount = $this->get_discount_detail($request->discount_code);
        $product = Product::where('id',$request->product_id)->first();

        $quantity = $request->quantity;
        $sum_total = ($product->current_price * $quantity);
        // Log::info($sum_total);
        if($discount)
        {   
            if($discount->discount_type == 1)
            {
                if($quantity >= 2){
                    $discount->discount_display = $discount->discount_amount.'%  (- RM'.(($sum_total*$discount->discount_amount)/100).')';
                    $discount->sum_total = number_format((float)(($product->current_price * $quantity)-(($sum_total*$discount->discount_amount)/100)+$request->shipping_rate), 2, '.', '');
                    // Log::info($discount->sum_total);
                }else{
                    $discount = ['error'=>'This discount code only available for 2 and more quantity'];
                }

            }
            
            else if($discount->discount_type == 2 )
            {
                if($sum_total >=100){
                    $discount->discount_display = $discount->discount_amount;
                    $discount->sum_total = number_format((float)($product->current_price * $quantity)-$discount->discount_amount,2,'.','');
                    // Log::info($discount->sum_total);
                }else{
                    $discount = ['error'=>'Only available for total over RM 100'];
                }
            }
            
        }
        else
        {
            $discount = ['error'=>'Code not available'];
        }
        
        return response()->json($discount);
    }

    public function summary($order_id,$guard = null){

        $order_info = Order_history::where('id',$order_id)
                    ->first();
        // Log::info($order_info->country);
        // $order_info = DB::table('order_history')
        //             ->join('product', 'product.id', '=', 'order_history.product_id')
        //             ->join('country', 'country.id', '=', 'order_history.country_id')
        //             ->select('product.*','order_history.*','country.*','order_history.id as oid','country.id as cid','product.name as productName','country.name as countryName')
        //             ->where('order_history.id','=',$order_id)
        //             ->get();

        // var_dump($order_info);
        return view('summary')->with('order_info',$order_info);
    }

    public function history(){
        $id = Auth::id();

        $order_history = Order_history::where('users_id',$id)
                        ->get();
        // Log::info($order_history->product);
        return view('history')->with('order_history',$order_history);
    }

    public function check_shipping(Request $request){



        if($request->country == 'none'){
            return ['error'=>'Please select a country'];
        }

        $discount = null;
        if($request->discount_code){
            $discount = Discount_code::where('name',$request->discount_code)->first();
            if($discount){
                $discount_rate = $discount->discount_amount;
            }else{
                return ['error'=>'Discount code not available'];            
            }
        }else{
            $discount_rate = 0;
        }
        // Log::info($discount);
        $product = Product::where('id',$request->product_id)->first();
        $country = Country::where('id',$request->country)->first();

        $price = $product->current_price;
        $quantity = $request->quantity;
        $sum_total = $price * $quantity;
        

        if(($sum_total > $country->free_delivery_amount && $country->free_delivery_amount_status == 1) || ($quantity >= $country->free_delivery_unit && $country->free_delivery_unit_status == 1))
        {
            $shipping_fee = 0;
        }
        else
        {
            $shipping_fee = $country->shipping_rate;
        }

        if(!is_null($discount))
        {
            if($discount->discount_type == 1){
                $total = (($price * $quantity) - (($price * $quantity)*$discount_rate/100))+$shipping_fee;
            }else if($discount->discount_type == 2){
                $total = (($price * $quantity) - $discount_rate)+$shipping_fee;
            }    
        }
        else
        {
            $total = (($price * $quantity))+$shipping_fee;
        }
        // Log::info();

        
        // Log::info($discount);
        $result = [
            'success'=>'success',
            'total'=>$total,
            'discount_rate' =>$discount_rate,
            'shipping_rate'=>$shipping_fee,
        ];
        // return response()->json($request);
        return response()->json($result);
    }

    public function get_discount_detail($param){
        $discount = Discount_code::where('name',$param)->first();
        return $discount;
    }
}
