<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class ProductController extends Controller
{

    public function index(){
    	$product_list = Product::all();

    	return view('product')->with('product_list',$product_list);
    }

}
