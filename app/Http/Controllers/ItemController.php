<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    public function create(Request $request)
    {
    	$item = new Item;
    	$item->name = 'ZOZ';
    	$item->price = '30';
    	$item->save();

    	$category = Category::find([1,2,3,4]);
    	$item->categories()->attach($category);

    	return 'success';
    }

    public function show(Item $item)
    {
    	return view('item.show',compact('item'));
    }
}
