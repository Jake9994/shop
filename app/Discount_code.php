<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount_code extends Model
{
	protected $table = 'discount_code';

    public function order_history(){
    	return $this->hasMany('App\Order_history');
    }
}
