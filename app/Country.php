<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	 protected $table = 'country';

    public function order_history(){
    	return $this->hasMany('Order_history');
    }
}
