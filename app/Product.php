<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

 	protected $table = 'product';

    public function order_history(){
    	return $this->hasMany('App\Order_history');
    }
}
