<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_history extends Model
{
    protected $table = 'order_history';

    protected $fillable = [
        'users_id', 'product_id', 'amount', 'discount_id','discount_amount', 'country_id','shipping_fee','total',
    ];

    public function users(){
    	return $this->belongsTo('App\Users');
    }

    public function product(){
    	return $this->belongsTo('App\Product', 'product_id', 'id');
    }

    public function discount_code(){
    	return $this->belongsTo('App\Discount_code');
    }

    public function country(){
    	return $this->belongsTo('App\Country');
    }
}
