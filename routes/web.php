<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('order_detail', function () {
//     // return 'GET';
//     return view('order_detail');
// });

// Route::post('order_detail', function () {
	
// 	if(empty($_POST)){
// 		return view('welcome');
// 	}else{
// 		var_dump($_POST);
// 		return view('order_detail');
// 	}
// });

// Route::get('/register','RegistrationController@create');
// Route::post('register','RegistrationController@store');

// Route::get('/login', 'SessionsController@create');
// Route::post('/login', 'SessionsController@store');
// Route::get('/logout', 'SessionsController@destroy');
Auth::routes();

Route::middleware(['auth'])->group(function(){
	
	Route::get('/home', 'ProductController@index')->name('home');
	Route::get('/product', 'ProductController@index')->name('product');
	Route::post('/order_detail','OrderController@order')->name('order_detail');
	Route::get('/order/{product_id}/{product_amount}','OrderController@order_info')->name('Order Info');
	Route::post('/p_check_discount','OrderController@check_discount')->name('check_discount');
	Route::post('p_check_out','OrderController@p_checkout')->name('process_checkout');

	Route::get('/summary/{order_id}','OrderController@summary');
	Route::get('/history','OrderController@history')->name('history');
	Route::post('/check_shipping','OrderController@check_shipping')->name('check_shipping');

});

// Testing Feature without using auth
Route::get('item/create', 'ItemController@create')->name('item.create');
Route::get('item/{item}', 'ItemController@show')->name('item.show');

Route::get('/email','EmailController@index')->name('email');
Route::post('/','EmailController@sendEmail')->name('sendmail');

